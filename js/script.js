/* Теоретичне питання
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX (Asynchronous JavaScript and XML) - це технологія, що дозволяє веб-сторінкам динамічно взаємодіяти з сервером без 
перезавантаження всієї сторінки. Вона корисна тим, що підвищує користувацький досвід, дозволяє ефективно оновлювати контент
в реальному часі та покращує продуктивність веб-додатків.

Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. 
Бажано знайти варіант на чистому CSS без використання JavaScript.*/


const API_URL = "https://ajax.test-danit.com/api/swapi/films"; 
const allEpisodes = document.getElementById('episodesList');

function getFilm () {
    fetch (API_URL, {
        method: 'GET'
    })
    .then((response) => response.json())
    .then((films) => {
        films.forEach ( film => {
            const episode = document.createElement ('li');
            episode.innerHTML = `
            <h2>${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <div id="characters-${film.episodeId}"></div>
            `;
            allEpisodes.append(episode);
            charactersFilm(film.episodeId, film.characters);
        });
    })
};

function charactersFilm(episodeId, characters) {
    const charactersContainer = document.getElementById(`characters-${episodeId}`);
    characters.forEach(url => {
        fetch(url, {
           method: 'GET'            
       })
            .then((response) => response.json()) 
            .then(character => {                
                const characterFilm = document.createElement('div');
                characterFilm.classList = 'characterFilm';
                characterFilm.innerHTML = `${character.name}`;
                charactersContainer.append(characterFilm);
            })
    })
}  

getFilm();